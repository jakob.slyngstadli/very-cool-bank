const laptopsElement = document.getElementById("laptops");
const priceElement = document.getElementById("price");
const addElement = document.getElementById("add");
const cartElement = document.getElementById("cart");
const quantityElement = document.getElementById("quantity");
const payButtonElement = document.getElementById("pay");
const totalDueElement = document.getElementById("totalDue");
const loanButtonElement = document.getElementById("loan");
const totalBankCash = document.getElementById("totalCash");
const payBackLoanButtonElement = document.getElementById("payLoan");
const setLoan = document.getElementById("loanText");
const doWorkButtonElement = document.getElementById("work");
const storeMoneyButtonElement = document.getElementById("workToBank");
const moneyViewElement = document.getElementById("earnedMoney");
const setFeatureTextElement = document.getElementById("featureText");

const setLaptopSpecsElement = document.getElementById("laptopSpecs");
const setLaptopImageElement = document.getElementById("laptopImage");
const setLaptopNameElement = document.getElementById("laptopName");

let laptops = [];
let cart = [];
let totalDue = 0;
let cashBank = 0;
let currentLoan = 0;
let currentSalary = 0;

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsForSale(laptops));

const getLoan = () => {
    const loanAmount = prompt("Please enter amount to loan: ");
    if(currentLoan === 0){
        if(parseInt(loanAmount) > 2 * cashBank){
            alert("Can't loan more than double your balance!! WORK!!")
        } else{
            cashBank += parseInt(loanAmount);
            currentLoan += parseInt(loanAmount);
            payBackLoanButtonElement.style.visibility="visible";
            setLoan.style.visibility="visible";
        }
    } else{
        alert("Can't get a loan when you owe moeney stinky")
    }
    
    totalBankCash.innerText = `Balance: ${cashBank} NOK`;
    setLoan.innerText = `Outstanding Loan: ${currentLoan} NOK`;
}

const handleLoanPayback = () => {
    if(currentSalary < currentLoan){
        alert("You dont have the funds to do this")
    } else if (currentSalary >= currentLoan){
        currentSalary -= currentLoan;
        currentLoan = 0;
        setLoan.style.visibility="hidden";
        payBackLoanButtonElement.style.visibility="hidden";
    } else{
        currentSalary = 0;
        currentLoan = 0;
        setLoan.style.visibility="hidden";
        payBackLoanButtonElement.style.visibility="hidden";
    }
    totalBankCash.innerText = `Balance: ${cashBank} NOK`;
    setLoan.innerText = `Outstanding Loan: ${currentLoan} NOK`;
    moneyViewElement.innerText = `Earned: ${currentSalary}`;
}

const doWork = () => {
    currentSalary += 100;
    moneyViewElement.innerText = `Earned: ${currentSalary}`;
}

const storeMoney = () => {
    if(currentLoan === 0){
        cashBank += currentSalary;
        currentSalary = 0;
    }
    else{
        currentLoan -= currentSalary*0.1;
        cashBank += currentSalary*0.9;
        currentSalary = 0;
    }
    totalBankCash.innerText = `Balance: ${cashBank} NOK`;
    setLoan.innerText = `Outstanding Loan: ${currentLoan} NOK`;
    moneyViewElement.innerText = `Earned: ${currentSalary}`;
}

const addLaptopsForSale = (laptops) => {
    laptops.forEach(laptop => addLaptopForSale(laptop));
    priceElement.innerText = `${laptops[0].price} NOK`;
    setFeatureTextElement.innerText = laptops[0].description;
    setLaptopNameElement.innerText = laptops[0].title;
    setLaptopSpecsElement.innerText = laptops[0].specs;
    // ER BUGGED
    document.getElementById("laptopImage").src= laptops[0].image;
    // Laptop image
}

const addLaptopForSale = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}

const handleLaptopSale = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    priceElement.innerText = `${selectedLaptop.price} NOK`;
    setFeatureTextElement.innerText = selectedLaptop.description;
    // laptop name
    // laptop specs
    // Laptop image
}

const handlePay = () => {
    if(parseInt(priceElement > cashBank)){
        alert("Not enough, please work more");
    } else{
        cashBank -= priceElement.innerText;    
    }
    totalBankCash.innerText = `Balance: ${cashBank} NOK`;
}

laptopsElement.addEventListener("change", handleLaptopSale);
payButtonElement.addEventListener("click", handlePay);
loanButtonElement.addEventListener("click", getLoan);
payBackLoanButtonElement.addEventListener("click", handleLoanPayback);
doWorkButtonElement.addEventListener("click", doWork);
storeMoneyButtonElement.addEventListener("click", storeMoney);